/****************************************************************************
 Copyright 2015 Apigee Corporation

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ****************************************************************************/
'use strict';

var config = require('../../../config');
var _ = require('lodash');
var path = require('path');
var fs = require('fs-extra');
var emit = require('../../util/feedback').emit;
var netutil = require('../../util/net');
var debug = require('debug')('swagger');
var util = require('util');
var cli = require('../../util/cli');
var template = require('swagger-test-templates');
var async = require('async');
var swaggerSpec = require('../../util/spec');
var spec = require('swagger-tools').specs.v2;
var inquirer = require('inquirer');
var hotp = require('otplib');
//var querystring = require('querystring');
//var https = require('https');
//var exec = require('child_process').execSync;

var FRAMEWORKS = {
  connect: { source: 'connect' },
  //express: { source: 'connect', overlay: 'express' },
  //hapi:    { source: 'connect', overlay: 'hapi' },
  //restify: { source: 'connect', overlay: 'restify' },
  //sails:   { source: 'sails' }
};

var TEST_ASSERTION_TYPES = ['expect', 'should', 'assert'];
var TEST_MODULES = ['supertest', 'request'];
var TEST_DEPENDENCIES = {
  'z-schema': '^3.12.0',
  request: '^2.58.0',
  chai: '^3.0.0',
  mocha: '^2.2.5',
  dotenv: '^1.2.0'
};

module.exports = {
  create: create,
  start: start,
  verify: verify,
  edit: edit,
  open: open,
  test: test,

  // for internal use
  frameworks: FRAMEWORKS,
  read: readProject,
  assertiontypes: TEST_ASSERTION_TYPES,
  testmodules: TEST_MODULES,

  // for testing stub generating
  generateTest: testGenerate,

  //additions for syndication
  generateRoutes: generateRoutes
};

//.option('-f, --framework <framework>', 'one of: connect | express')
function create(name, options, cb) {

  //validate name
  if (name) {
    var targetDir = path.resolve(process.cwd(), name);
    try {
      fs.statSync(name);
      if (fs.isDirectory()) {
        return cb(new Error('Directory ' + targetDir + ' already exists.'));
      }
    } catch(e) {
      //ignore ENOENT
    }
  }

  var questions = [
    { name: 'name', message: 'Project name?', validate: function(name) {
        try {
          fs.statSync(name);
          if (fs.isDirectory()) {
            return false;
          }
        } catch (e) {
          //ignore ENOENT
        }
        return true;
      }
    },
    //{ name: 'framework', message: 'Framework?', type: 'list', choices: Object.keys(FRAMEWORKS) }
  ];

  var results = {
    name: name,
    framework: options.framework
  };

  cli.requireAnswers(questions, results, function(results) {

    var name = results.name;
    var framework = "connect";
    var targetDir = path.resolve(process.cwd(), name);

    cloneSkeleton(name, framework, targetDir, function(err) {
      if (err) { return cb(err); }
      emit('Project %s created in %s', name, targetDir);
      var fsx = require('fs');
      var dfile = fsx.readFileSync(targetDir + '/Dockerfile', 'utf8');
      dfile = dfile.replace(/\[repo name\]/g, name);
      fsx.writeFileSync(targetDir + '/Dockerfile', dfile, 'utf8');
      var message = util.format('Success! You may edit your new api by running: "swagg edit %s"', name);

      installDependencies(targetDir, message, cb);
    });
  });
}

//.option('-d, --debug [port]', 'start in remote debug mode')
//.option('-b, --debug-brk [port]', 'start in remote debug mode, wait for debugger connect')
//.option('-m, --mock', 'start in mock mode')
//.option('-o, --open', 'open in browser')
function start(directory, options, cb) {

  readProject(directory, options, function(err, project) {
    if (err) { throw err; }

    var fullPath = path.join(project.dirname, project.api.main);
    emit('Starting: %s...', fullPath);
    var nodemonOpts = {
      script: project.api.main,
      ext: 'js,json,yaml,coffee',
      nodeArgs: []
    };
    if (project.dirname) { nodemonOpts.cwd = project.dirname; }
    if (options.debugBrk) {
      var debugBrkArg = '--debug-brk';
      if (typeof options.debugBrk === 'string') {
        debugBrkArg += '=' + options.debugBrk;
      }
      nodemonOpts.nodeArgs.push(debugBrkArg);
    }
    if (options.debug) {
      var debugArg = '--debug';
      if (typeof options.debug === 'string') {
        debugArg += '=' + options.debug;
      }
      nodemonOpts.nodeArgs.push(debugArg);
    }
    if (options.nodeArgs) {
      nodemonOpts.nodeArgs = nodemonOpts.nodeArgs.concat(options.nodeArgs.split(' '));
    }
    // https://www.npmjs.com/package/cors
    nodemonOpts.env = {
      NODE_ENV: 'development',
      swagger_corsOptions: '{}' // enable CORS so editor "try it" function can work
    };
    if (options.mock) {
      nodemonOpts.env.swagger_mockMode = true;

    }
    var nodemon = require('nodemon');
    //defaults to no HOTPsecurity, snakeoil ssl cert
    nodemonOpts.env.NODE_ENV = "development";
    // hack to enable proxyquire stub for testing...
    if (_.isFunction(nodemon)) {
      nodemon(nodemonOpts);
    } else {
      nodemon._init(nodemonOpts, cb);
    }

    nodemon.on('start', function () {
      emit('  project started here: ' + project.api.localUrl);
      emit('  project will restart on changes.');
      emit('  to restart at any time, enter `rs`');

      if (options.open) {
        setTimeout(function() {
          open(directory, options, cb);
        }, 500);
      }
    }).on('restart', function (files) {
      emit('Project restarted. Files changed: ', files);
    });
  });
}

//.option('-d, --debug [port]', 'start in remote debug mode')
//.option('-b, --debug-brk [port]', 'start in remote debug mode, wait for debugger connect')
//.option('-m, --mock', 'start in mock mode')
//.option('-o, --open', 'open in browser')
function test(directory, options, cb) {

  var Mocha = require('mocha');
  var MochaUtils = require('mocha/lib/utils');

  readProject(directory, options, function(err, project) {

    if (err) { return cb(err); }

    var mocha = new Mocha({
      timeout: '10000',
    });
    var testPath = project.dirname;
    if (directory) {
      try {
        testPath = fs.realpathSync(directory);
      } catch (err) {
        return cb(new Error(util.format('no such file or directory %s', directory)));
      }
    }
    testPath = path.resolve(testPath, 'test');
    debug('testPath: %s', testPath);

    if (fs.statSync(testPath).isFile()) {
      if (testPath.substr(-3) !== '.js') { return cb(new Error('file is not a javascript file')); }
      mocha.addFile(testPath);
      debug('mocha addFile: %s', testPath);
    } else {
      MochaUtils.lookupFiles(testPath, ['js'], true)
        .forEach(function(file) {
          mocha.addFile(file);
          debug('mocha addFile: %s', file);
        });
    }

    emit('Running tests in: %s...', testPath);

    if (options.mock) {
      process.env.swagger_mockMode = true;
    }

    process.env.NODE_ENV = 'test';
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    mocha.run(function(failures) {
      cb(null, failures);
    });
  });
}

function verify(directory, options, cb) {

  readProject(directory, options, function(err, project) {
    if (err) { return cb(err); }

    swaggerSpec.validateSwagger(project.api.swagger, options, cb);
  });
}

function edit(directory, options, cb) {

  readProject(directory, options, function(err, project) {
    if (err) { return cb(err); }
    var editor = require('./swagger_editor');
    editor.edit(project, options, cb);
  });
}

function open(directory, options, cb) {

  readProject(directory, options, function(err, project) {
    if (err) { return cb(err); }

    netutil.isPortOpen(project.api.port, function(err, isOpen) {
      if (err) { return cb(err); }
      if (isOpen) {
        var browser = require('../../util/browser');
        browser.open(project.api.localUrl, cb);
      } else {
        emit('Project does not appear to be listening on port %d.', project.api.port);
      }
    });
  });
}

// Utility

function readProject(directory, options, cb) {

  findProjectFile(directory, options, function(err, fileName) {
    if (err) { return cb(err); }

    var yaml = require('js-yaml');
    var Url = require('url');

    var string = fs.readFileSync(fileName, { encoding: 'utf8' });
    var project = JSON.parse(string);

    project.filename = fileName;
    project.dirname = path.dirname(fileName);

    if (!project.api) { project.api = {}; }

    project.api.swaggerFile = path.resolve(project.dirname, config.swagger.fileName);
    project.api.swagger = yaml.safeLoad(fs.readFileSync(project.api.swaggerFile, 'utf8'));

    project.api.name = project.name;
    project.api.main = project.main;
    project.api.host = project.api.swagger.host;
    project.api.basePath = project.api.swagger.basePath;

    project.api.localUrl = 'http://' + project.api.host + project.api.swagger.basePath;
    project.api.port = Url.parse(project.api.localUrl).port || 80;

    debug('project.api: %j', _.omit(project.api, 'swagger'));
    cb(null, project);
  });
}

// .option('-p, --project', 'use specified project file')
function findProjectFile(startDir, options, cb) {

  var parent = startDir = startDir || process.cwd();
  var maxDepth = 50;
  var current = null;
  while (current !== parent && maxDepth-- > 0) {
    current = parent;
    var projectFile = path.resolve(current, 'package.json');
    if (fs.existsSync(projectFile)) {
      return cb(null, projectFile);
    }
    parent = path.join(current, '..');
  }
  cb(new Error('Project root not found in or above: ' + startDir));
}

function cloneSkeleton(name, framework, destDir, cb) {

  var skeletonDir = config.project.skeletonDir;

  framework = FRAMEWORKS[framework];
  var sourceDir = path.resolve(skeletonDir);
  var overlayDir = (framework.overlay) ? path.resolve(skeletonDir, framework.overlay) : null;

  var done = function(err) {
    if (err) { return cb(err); }
    customizeClonedFiles(name, framework, destDir, cb);
  };

  debug('copying source files from %s', sourceDir);
  fs.copy(sourceDir, destDir, true, function(err) {
    if (err) { return cb(err); }
    if (overlayDir) {
      debug('copying overlay files from %s', overlayDir);
      fs.copy(overlayDir, destDir, false, done);
    } else {
      done();
    }
  });
}

function customizeClonedFiles(name, framework, destDir, cb) {

  // npm renames .gitignore to .npmignore, change it back
  var npmignore = path.resolve(destDir, '.npmignore');
  var gitignore = path.resolve(destDir, '.gitignore');
  fs.rename(npmignore, gitignore, function(err) {
    if (err && !fs.existsSync(gitignore)) { return cb(err); }

    // rewrite package.json
    var fileName = path.resolve(destDir, 'package.json');
    fs.readFile(fileName, { encoding: 'utf8' }, function(err, string) {
      if (err) { return cb(err); }

      var project = JSON.parse(string);
      project.name = name;

      debug('writing project: %j', project);
      fs.writeFile(fileName, JSON.stringify(project, null, '  '), cb);
    });
  });
}

function spawn(command, options, cwd, cb) {

  var cp = require('child_process');
  var os = require('os');

  var isWin = /^win/.test(os.platform());

  emit('Running "%s %s"...', command, options.join(' '));

  var npm = cp.spawn(isWin ?
                       process.env.comspec :
                       command,
                     isWin ?
                       ['/c'].concat(command, options) :
                       options,
                     { cwd: cwd });
  npm.stdout.on('data', function (data) {
    emit(data);
  });
  npm.stderr.on('data', function(data) {
    emit('%s', data);
  });
  npm.on('close', function(exitCode) {
    if (exitCode !== 0) { var err = new Error('exit code: ' + exitCode); }
    cb(err);
  });
  npm.on('error', function(err) {
    cb(err);
  });
}


//.option('-p, --path-name [path]', 'a sepecific path of the api')
//.option('-f, --test-module <module>', 'one of: ' + testmodules)
//.option('-t, --assertion-format <type>', 'one of: ' + assertiontypes)
//.option('-o, --force', 'allow overwriting of all existing test files matching those generated')
// TODO: this needs inspection
function testGenerate(directory, options, cb) {
  var pathList = [];
  var desiredPaths = [];
  var testModule = options.testModule || TEST_MODULES[0];
  var assertionFormat = options.assertionFormat || TEST_ASSERTION_TYPES[0];
  var overwriteAll = options.force || false;
  var loadTesting = options.loadTest || false;
  directory = directory || process.cwd();

  findProjectFile(directory, null, function(err, projPath) {
    var projectFile;
    var projectJson = require(projPath);
    var jsonCopy = _.cloneDeep(projectJson);
    var runInstall = false;

    if (err) { return cb(err); }

    if (!_.isEqual(jsonCopy.devDependencies, _.defaultsDeep(projectJson.devDependencies, TEST_DEPENDENCIES))) {
      runInstall = true;
    }

    _.defaultsDeep(projectJson, {scripts: {test: 'swagger project test'}});

    projectFile = JSON.stringify(projectJson, null, 2);

    fs.writeFileSync(projPath, projectFile);

    if (!fs.existsSync(path.join(directory, 'test/api/client'))) {
      fs.mkdirSync(path.join(directory, 'test/api/client'));
    }

    //read the yaml file and validate it
    readProject(directory, options, function(err, project) {
      if (err) { return cb(err); }
      swaggerSpec.validateSwagger(project.api.swagger, options, function(err) {
        spec.resolve(project.api.swagger, function(err, result) {
          // get the array of string paths from json object
          pathList = Object.keys(result.paths);

          //check if the test frame is one of the two
          if (options.testModule && !_.includes(TEST_MODULES, options.testModule)) {
            return cb(new Error(util.format('Unknown type: %j. Valid types: %s', options.testModule, TEST_MODULES.join(', '))));
          }

          // check if the assertion-format is one of the three
          if (options.assertionFormat && !_.includes(TEST_ASSERTION_TYPES, options.assertionFormat)) {
            return cb(new Error(util.format('Unknown type: %j. Valid types: %s', options.assertionFormat, TEST_ASSERTION_TYPES.join(', '))));
          }

          // process the paths option
          if (options.pathName){
            var reg = new RegExp(options.pathName);
            desiredPaths = pathList.filter(function(val) {
              return val.match(reg);
            });
          }

          // pass the config to the module and get the result string array
          var config = {
            pathName: desiredPaths,
            testModule: testModule,
            assertionFormat: assertionFormat
          };

          // pass list of paths targeted for load testing
          if (loadTesting) {
            if ((typeof loadTesting) !== 'boolean' && fs.existsSync(path.join(directory, loadTesting))) {
              config.loadTest = parseJsonFile(directory, loadTesting).loadTargets;
            } else if (fs.existsSync(path.join(directory, 'load-config.json'))){
              config.loadTest = parseJsonFile(directory, 'load-config.json').loadTargets;
            } else {
              return cb(new Error('Config file not found. Please specify a load test config or add load-config.json file to your project directory.'));
            }
          }

          var finalResult = template.testGen(result, config);
          var existingFiles = fs.readdirSync(path.join(directory, 'test/api/client'));
          var skipAll = false;

          async.filterSeries(finalResult, function(file, cb) {
            if (overwriteAll) {
              cb(true);
            } else if(skipAll){
              cb(false);
            } else {
              if (_.includes(existingFiles, file.name)) {
                var prompt = util.format('Conflict on %s. Overwrite? (answer \'h\' for help):', file.name);
                var question = {type: 'expand', message: prompt, name: 'overwrite', choices: [
                  {
                    key: "y",
                    name: "Overwrite this one and show the next",
                    value: "overwrite"
                  },
                  {
                    key: "a",
                    name: "Overwrite this one and all of the next",
                    value: "overwrite_all"
                  },
                  {
                    key: 'n',
                    name: 'Skip this one and show the next',
                    value: 'overwrite_skip'
                  },
                  {
                    key: 'x',
                    name: 'Skip this one and all of the next',
                    value: 'overwrite_skip_all'
                  }
                ]};

                inquirer.prompt(question, function(answers) {
                  if (answers.overwrite === 'overwrite') {
                    cb(true);
                  } else if (answers.overwrite === 'overwrite_all') {
                    overwriteAll = true;
                    cb(true);
                  } else if (answers.overwrite === 'overwrite_skip') {
                    cb(false);
                  } else {
                    skipAll = true;
                    cb(false);
                  }
                });
              } else {
                cb(true);
              }
            }
          }, function(filteredResult) {

            async.each(filteredResult, function(file, cb) {
              if (file.name === '.env') {
                fs.outputFile(path.join(directory, file.name), file.test, cb);
              } else {
                fs.outputFile(path.join(directory, '/test/api/client', file.name), file.test, cb);
              }
            }, function(err) {
              if (runInstall) {
                installDependencies(directory, 'Success! You may now run your tests.', cb);
              }
            });
          });
        });
      });
    });
  });
}

function parseJsonFile(directory, filePath) {
  return JSON.parse(fs.readFileSync(path.join(directory, filePath)));
}

function installDependencies(directory, message, cb) {
  spawn('npm', ['install'], directory, function(err) {
    if (err) {
      emit('"npm install" failed. Please run "npm install" in %s.', directory);
      return cb(err);
    }
    cb(null, message);
  });
}


var path = require("path");
//renders some simple boilerplate for each endpoint specified, adds controllers/operations
function generateRoutes(directory, options, cb) {
  try {
    var swaggerFileName = path.resolve(directory || process.cwd(), config.swagger.JSONfileName);

    emit("Processing " + swaggerFileName + "...");
    //note: must convert to string first from buffer?
    var swagDoc = JSON.parse(fs.readFileSync(swaggerFileName));
    if ('paths' in swagDoc) {
      emit("Paths found!");
      for (var p in swagDoc.paths) {
        if ('x-swagger-router-controller' in swagDoc.paths[p]) {
          var controllerName = swagDoc.paths[p]["x-swagger-router-controller"];

          //write the controller
          var controllerPath = path.resolve(directory || process.cwd(), "api/controllers/", controllerName+".js");
          var testPath = path.resolve(directory || process.cwd(), "test/", controllerName+".js");

          try {
              fs.accessSync(controllerPath, fs.F_OK);
              var controllerPathNew = path.resolve(directory || process.cwd(), "api/controllers/", controllerName+"_new.js");
              emit("Existing controller found at " + controllerPath);
              emit("Generating at " + controllerPathNew + " instead. Use git diff to compare them.");
              controllerPath = controllerPathNew;
          } catch (e) {
              //ignore inaccessability. Yes I know this is a bad pattern but that's what node's APIs have left me with
          }

          try {
              fs.accessSync(testPath, fs.F_OK);
              var testPathNew = path.resolve(directory || process.cwd(), "test/", controllerName+"_new.js");
              emit("Existing test found at " + testPath);
              emit("Generating at " + testPathNew + " instead. Use git diff to compare them.");
              testPath = testPathNew;
          } catch (e) {
              //ignore inaccessability. Yes I know this is a bad pattern but that's what node's APIs have left me with
          }

          var controller = generateController(swagDoc.paths[p]);
          var test = generateTest(controllerName, swagDoc.paths[p]);


          fs.writeFileSync(controllerPath,controller);
          emit("Wrote controller: "+controllerName+"");
          fs.writeFileSync(testPath,test);
          emit("Wrote test: " + testPath);
        }

      }
    }
    emit("Route generation complete.");
  } catch (err) {
    emit("Couldn't autogen routes due to: " + err + " (probably problem with swagger.json)");
  }
}

function generateTest(name, operations) {
  var con = "/*\nTesting the functionality of " + name + "\n";
      con += "*/\n\n";

      con += "var " + name + " = require('../api/controllers/" + name + ".js');\n\n";

      con += "describe('" + name + "', function() {\n";


      for (var op in operations) {
        if (op.indexOf('x-') < 0) {//ensure we're not making routes for extensions (like x-router-controller)
          var name = op;
          if ('x-swagger-operationId' in operations[op]) {
            name = operations[op].operationId;
          }

          con += "\tdescribe('Operation: " + name + "', function() {\n\n";
          con += "\t\t// Remove the following test when you actually write a test.\n";
          con += "\t\tit('should have at least one working test', function() {\n\t\t\tthrow new Error('no test defined');\n\t\t});\n\n";
          con += "\t});\n";

        }

      }
  con += "});"
  return con;
}

function generateController(operations) {
  //create controller and add the appropriate operations by id or method as specified
  var con = '"use strict";\n';
      con += 'var controller = module.exports;\n\n';

      con += '/**\n';
      con += ' * The Request Object is a standard node http.IncomingMessage, with a swagger attribute\n';
      con += ' * @typedef {Object} RequestObject \n';
      con += ' * @property {SwaggerAddon} swagger \n';
      con += ' * @property {Object}       headers - Object with header names as keys and values as values.\n';
      con += ' * @property {string}       httpVersion - String containing http version (probably 1.1). \n';
      con += ' * @property {string}       method - String containing method type (probably "GET" or "POST"). \n';
      con += ' * @property {string}       url - String containing the raw URL from the raw HTTP request. (example: "/path1/path2/file.txt")\n';
      con += ' */\n\n';

      con += '/**\n';
      con += ' * The structure of req.swagger \n';
      con += ' * @typedef {Object} SwaggerAddon \n';
      con += " * @property {string}   apiPath             - The API's path (The key used in the paths object for the corresponding API). \n";
      con += " * @property {Object}   path                - The corresponding path in the Swagger object that the request maps to. \n";
      con += " * @property {Object}   operation           - The corresponding operation in the API Declaration that the request maps to. \n";
      con += " * @property {Object[]} operationParameters - The computed parameters for this operation. \n";
      con += " * @property {string[]} operationPath       - The path to the operation in string array format. \n";
      con += " * @property {Object}   params              - For each of the request parameters defined in your Swagger document, its path, its schema and its processed value. (In the event the value needs coercion and it cannot be converted, the value property will be the original value provided.) \n";
      con += " * @property {Object[]} security            - The computed security for this request \n";
      con += " * @property {Object}   swaggerObject       - The Swagger full swagger Document Object \n";
      con += " */";
  //extract code from existing controller, if there is one
  for (var op in operations) {
    if (op.indexOf('x-') < 0) {//ensure we're not making routes for extensions (like x-router-controller)
      var name = op;
      if ('x-swagger-operationId' in operations[op]) {
        name = operations[op].operationId;
      }

      con += "\n\n/**\n * Operation "+name+": "+(operations[op]['description']||"DESCRIPTION GOES HERE")+"\n";
      con += " * @param {RequestObject}  req - The Request Object \n";
      con += " * @param {Object}         res - The Response Object (see https://nodejs.org/api/http.html#http_class_http_serverresponse) \n";
      if ('parameters' in operations[op]) {
        con += ' * req.swagger.params parameters expected:\n';
        for (var p in operations[op].parameters) {
          con += ' *   '+operations[op].parameters[p]["name"]+ "("+(operations[op].parameters[p]["required"]?"Required":"Optional")+")\n";
        }
      }
      con += ' */\n';
      con += 'controller["'+name+'"] = function(req,res) {\n\t//YOUR CODE GOES HERE\n\tres.statusCode = 501;\n\tres.end(\'{"error":"Not implemented yet."}\');\n};';

      //TODO maybe add new operations if the controller already existed? might be a bit hard (possibly append new ones only by tracking what changes have been made?)
    }

  }
  return con;
}

/*
function generateCSR(directory, options, cb) {
  try {
  var sslDir = path.resolve(directory || process.cwd(), "ssl");
    exec("openssl genrsa -out key.key 2048", {cwd: sslDir});
    exec("openssl req -new -key key.key -out csr.csr", {cwd: sslDir});
  } catch (e) {

  }


}

function publish(directory, options, cb) {
  try {
    var swaggerFileName = path.resolve(directory || process.cwd(), config.swagger.JSONfileName);
    var secretKeyFile = path.resolve(directory || process.cwd(), "secretKey.js");
    //note: must convert to string first from buffer?
    var swagDoc = fs.readFileSync(swaggerFileName);
    var secret = require(secretKeyFile);

    var data = querystring.stringify({
      swagger: swagDoc,
      HOTPKey: secret
    });
    https.request({
      host: options.to || "api.colab.duke.edu",
      port: 443,
      path: "/meta/v1/apis",
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Content-Length": Buffer.byteLength(data)
      }
    }, function(res) {
      emit("Successfully published!");
    }).write(data);

  } catch (err) {
    emit("Couldn't publish due to: " + err);
  }
}*/
