# Swagg

This module allows you to generate data source apis to be securely syndicated by api.colab.

# Your swagger API in five steps

## 1. Install the swagger module

Just do the following:

```bash
$ # Ensure node v6 (v4 should also work) is already installed
$ npm install -g swagg
```

## 2. Create a new swagger project

Use the CLI to create and edit projects.

```bash
$ swagg create ProjectName
```

## 3. Design your API in the Swagger Editor

The interactive, browser-based [Swagger Editor](http://editor.swagger.io/) is built in. Simply start the editor below, and it'll bring up a browser where you can edit your swagger doc. When you're done editing, just close your browser and use ctrl-c to kill the edit process.

```bash
$ swagg edit path/to/the/project
```

or

```bash
# From the project root directory
$ swagg edit
``` 


The platform allows you to generate controllers automatically based on the Swagger Doc you put together. If you look at the Swagger file in the editor, the `x-swagger-router-controller` element specifies the name of the controller file associated with the `/hello` path. The `x-swagger-router-controller` is mandatory for each path. For example:

```yaml
    paths:
        /hello:
            x-swagger-router-controller: example_contoller
```

The `operationId` is an optional element specifies which controller function to call. Without it, the router looks for a function of the same name as the HTTP Method for the operation (ie "get","post"). In this case, it is a function called `hello`. If the `operationId` were omitted, the function would be `get`.

```yaml
    paths:
        /hello:
            post:
                x-operationId: hello
```


Controller source code is always placed in `./api/controllers`. So, the controller source file for this project is `./api/controllers/hello_world.js`. 


## 4. Generate controllers and tests for your API

To generate controllers, the meat of your API, simply run:

```bash
# From the project root directory
$ swagg generate-routes
```
 or
 
```bash
$ swagg generate-routes path/to/the/project
```

This will also generate tests under `test/`. These tests are set to fail by default, so you should replace them immediately with actual tests of the behavior of your routes!

## 5. Write controller and test code in Node.js

The generator gives you easy entry points for your API. All you have to do is fill in the blanks. Code your API's business logic in Node.js.

excerpt from the generated `example_controller.js` controller
```js
// Operation get
// Parameters expected:
// name(Optional)
controller["hello"] = function(req,res) {
    
    //YOUR CODE GOES HERE
    // Example:
    // variables defined in the Swagger document can be referenced using req.swagger.params.{parameter_name}
    var name = req.swagger.params.name.value || 'stranger';
    var hello = util.format('Hello, %s!', name);
    // this sends back a JSON response which is a single string
    res.json(hello);
    
};
```

The tests generated correspond directly to the routes that you code. Feel free to write tests before you code (for Test-Driven Dev), or afterwards, for more traditional dev, but be sure to write them. They are initialized to fail, which prevents the Gitlab CI from deploying your api until you have working tests. They look like this when first initialized:

```js
/*
Testing the functionality of example_controller
*/

var example_controller = require('../api/controllers/example_controller.js');

describe('example_controller', function() {
	describe('Operation: get', function() {

		// Remove the following test when you actually write in a test.
		it('should have at least one working test', function() {
			throw new Error('no test defined');
		});

	});
});

```


## 5. Debug the server

Run the project server. Confirm that all your endpoints function as you expect. Running the server with the following command will run it without any of the security protections provided through the middleman server, and will use snakeoil ssl certificates. The way it's set up by default, your api will actually restart automatically as you make changes to its source so you can see them in realtime. Here is where you debug and build your endpoints!

```bash
# From the project root directory
$ swagg start
```

 or

```bash
$ swagg start path/to/the/project
```

### 5. Making changes

You'll probably at some point want to go back and change the structure of the API. This can be done by:

1. Using `swagg edit` again to change the spec
2. Using `swagg generate-routes` again to regenerate routes. This will result in a second copy of any routes that were not added/removed. You must merge them together to get the newly generated code without removing your old code. `diff` is your friend here! 

## 6. Deploy your new API!

Just push it to Gitlab, as usual, and it should work like a charm!
## <a name="about"></a>About this project

Apigee donated the code to create the swagger-node project in 2015. _This branch has been forked from the original for use in generating Duke's API systems by Jason "Toolbox" Oettinger._

 >Copyright 2015 Apigee Corporation

 >Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 >http://www.apache.org/licenses/LICENSE-2.0

 >Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
