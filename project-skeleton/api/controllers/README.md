#Swagger Controllers

In order to actually write your API, your swaggerDoc needs routing information added to it.

##x-router-controller and operationId

__x-router-controller__ can be defined at either the path or the operation level, and describes the routing file that should be used for the path/operation. Operation-defined x-router-controllers can override those defined at the path level. If _/path/endpoint_ contained an __x-router-controller__ value at the path level of _endpoint_, the _endpoint.js_ file in the _api/controllers_ directory would then be searched for the function for that operation. For each request, an operation is routed to the controller as specified, and is directed to a function named for the HTTP method chosen. For example, a request such as `GET /path/endpoint` would be routed to the _get()_ function of its controller file. The functions are standard connect/http functions, receiving a request and response parameter. If you wish to customize the function names, simply specify an __operationId__ at the operation level, and the router will use this as the function name for that operation. 

###Example

__api/swagger/swagger.json__

```json
{
	"info" : { ... }
	"paths": {
		"/hello": {
				"x-router-controller": "hello"
				get: { ... }
				delete: {
					"operationId": "deleteMe"
				}
		}
	}
}
```

__api/controllers/hello.js__

```javascript
module.exports = {
	"get": function(req,res) {
		res.end("you got me!");
	},
	"deleteMe": function(req,res) {
		res.end("deleted...");
	}
}
 
```