const request = require('request');
const { expect } = require('chai');

describe('/endpoint_1', () => {
  // start server and wait for it's actual LISTEN event.
  before((done) => {
    require('./../../../index.js')
      .then(() => {
        done();
      });
  });

  it('should pass dummy test', (done) => {
    done();
  });

  // it('should hit the endpoint!', (done) => {
  //   request.get('https://localhost:3000/v1/endpoint_1', (err, res, body) => {
  //     expect(err).to.be.null;
  //     expect(res.statusCode).to.equal(200);
  //     expect(body).to.be.not.null;
  //     const data = JSON.parse(body);
  //     expect(data).to.have.property('property 1');
  //     done();
  //   });
  // });
});
