'use strict';
//set debug mode and test mode if debug in the command
for (var i = 0; i < process.argv.length; i++) {
  var DEV = process.env.NODE_ENV === 'development' || process.argv[i] === "debug" || DEV;
  var TEST = process.env.NODE_ENV === 'test' || TEST;
}
//allow viewing of the debug console when in debug mode
if (DEV) {
  console.log("Debugging Mode Engaged...");
  process.env["DEBUG"] = 'connect:*';
}
var fs = require('fs');
var https = require('https');
var SwaggerTools = require('swagger-tools');
var signature = require('http-signature');

var config = {
  appRoot: __dirname // required config
};

var swaggerDoc = require('./api/swagger/swagger.json');

var app = require('connect')();
if (process.env.NODE_ENV === 'production') {
  var rollbar = require('rollbar');
  var { POST_SERVER_ITEM_ACCESS_TOKEN } = require('./credentials/');
  rollbar.handleUncaughtExceptions(POST_SERVER_ITEM_ACCESS_TOKEN);
}

// Promise-ify server starting for easy testing
module.exports = new Promise((resolve, reject) => {
  SwaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {

    // add hmac validation to only allow api syndication server access
    if (!DEV && !TEST) {
      app.use(check_signatures);
    }

    //only allow json resposes
    app.use(setJSONFormat);

    // install swagger middleware
    app.use(middleware.swaggerMetadata());
    //app.use(middleware.swaggerSecurity()); <= also not using this one
    app.use(middleware.swaggerValidator());
    app.use(catchValidationErrors);//elegantly catch validation errors
    app.use(middleware.swaggerRouter({
      controllers: './api/controllers',
      useStubs: false //process.env.NODE_ENV === 'development' ? true : false
    }));

    var key = DEV || TEST ? fs.readFileSync('./ssl/snakeoil.key') : fs.readFileSync('/srv/ssl/[service name].key');
    var cert = DEV || TEST ? fs.readFileSync('./ssl/snakeoil.cert') : fs.readFileSync('/srv/ssl/[service name].cert');

    https.createServer({ key: key, cert: cert }, app).listen(3000, function () {
      console.log('Listening on port ' + 3000);
      if (process.env.NODE_ENV === 'production') {
        rollbar.reportMessage(`[service name] online`, "info");
      }
      resolve();
    });
  });
});


function check_signatures(req, res, next) {
  if (signature.valid(req)) {
    next();
  } else {
    res.writeHead(403, { "Content-Type": "application/json" });
    res.end('{"error":"Unauthorized access. Signature invalid. You must go through the syndication server."}');
  }
}

function setJSONFormat(req, res, next) {
  res.setHeader('content-type', 'application/json');
  next();
}

function catchValidationErrors(err, req, res, next) {
  if ('failedValidation' in err && err.failedValidation) {
    res.statusCode = 400;
    res.end('{"error":"The request failed validation of its parameters and format. Check to make sure it conforms to the swagger spec for this api."}');
  } else {
    next();
  }
}
